FROM alpine

RUN apk add --no-cache openssl xxd zip
COPY crxmake.sh /usr/bin/crxmake

CMD ["crxmake.sh"]